from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('thanks/', views.IndexView.as_view()),
    path('login/', views.signin, name='login'),
    path('workspace/', views.workspace, name="workspace"),
    path('imports/', views.imports, name='imports'),
    path('logout/', views.logout_view, name="logout")
]