import logging
from dougal.forms import DatasetForm
from dougal.services.facade import DataHandler

from celery import shared_task
from gust.celery import app

logger = logging.getLogger()
data_handler = DataHandler()

################################################################
# Helpers for async tasks
################################################################
@app.task
def add_analytic_job(users, events, visits):
    logger.info('adding analytic job for user: %s', visits)
    try:
        data_handler.process_dataset(users, events, visits)
    except Exception as exc:
        logger.error('add_analytic_job() - error on data processing: %s', exc)