from django.apps import AppConfig


class DougalConfig(AppConfig):
    name = 'dougal'
