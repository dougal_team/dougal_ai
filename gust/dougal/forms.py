from django import forms
from dougal.models import DatasetModel

class ContactForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)

    def send_email(self):
        pass

class DatasetForm(forms.ModelForm):
    class Meta:
        model = DatasetModel
        fields = ('users_doc', 'events_doc', 'visits_doc')