from django.contrib import admin
from .models import ContactModel, DatasetModel

# Register your models here.
admin.site.register(ContactModel)
admin.site.register(DatasetModel)