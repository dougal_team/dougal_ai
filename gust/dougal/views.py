import logging
import celery
from django.http import HttpResponse
from django.views.generic import CreateView, FormView
from dougal.models import ContactModel
from django.contrib.auth import authenticate
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse

from dougal.forms import DatasetForm
from dougal.services.facade import DataHandler
from .tasks import add_analytic_job

logger = logging.getLogger()



class IndexView(CreateView):
    template_name = "dougal/index.html"
    success_url = 'thanks/'
    model = ContactModel
    fields = ['name', 'email', 'message']

def signin(request):
    if request.POST:
        name = request.POST.get("email")
        password = request.POST.get("password")
        user = authenticate(username=name, password=password)

        if user is not None:
            login(request, user)

            if request.POST.get('rememb-check'):
                request.session.set_expiry(0)

            return redirect("/workspace")
        else:
            print('no user')
            context = {"error_message": "Oooo..."}
            return render(request, 'dougal/login.html', context)

    else:
        return render(request, 'dougal/login.html', {})

def logout_view(request):
    logout(request)
    return redirect("/")

@login_required(login_url='/login/')
def workspace(request):
    return render(request, "dougal/workspace.html")
    
@login_required(login_url='/login/')
def imports(request):
    if request.method == "POST":
        dataset = DatasetForm(request.POST, request.FILES)

        if dataset.is_valid():
            item = dataset.save(commit=False)
            item.owner = request.user
            item.save()

            add_analytic_job.delay(item.users_doc.path, item.events_doc.path, item.visits_doc.path)

            context = {'success_message': 'Зміни завантажені!'}
            return render(request, 'dougal/imports.html', context)
        
        context = {'error_message': 'Помилка при збереженні даних!'}
        return render(request, 'dougal/imports.html')
    
    elif request.method == "GET":
        return render(request, 'dougal/imports.html')


    
    





