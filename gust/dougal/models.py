from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ContactModel(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    message = models.TextField()

    def send_email(self):
        pass

    def __str__(self):
        return self.name


class DatasetModel(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    users_doc = models.FileField(upload_to='dataset/users/')
    events_doc = models.FileField(upload_to='dataset/events/')
    visits_doc = models.FileField(upload_to='dataset/visits/')
    is_processed = models.BooleanField(default=False)

    def __str__(self):
        return self.owner
