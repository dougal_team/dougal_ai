import logging

logger = logging.getLogger()

class RecomendationManager:
    """
    Responsible for building matrix factorization model
    """
    
    def __init__(self):
        logger.info("RecomendationManager - created")

    def train_model(user_id):
        logger.info("Training model for %s", user_id)
        return True
