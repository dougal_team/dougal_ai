import math
import numpy as np
import pandas as pd
import keras
import matplotlib.pyplot as plt
import logging
import gc
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint
from keras.layers import Embedding, Reshape, Dot, Flatten, Input
from keras.layers import Multiply
from keras.models import Sequential, Model
from keras.layers import dot, add
from keras.backend.tensorflow_backend import set_session, clear_session, get_session

logger = logging.getLogger(__name__)

class RecomendationEngineNN(object):
    """
    Deep learning model for recomendation system
    """
    # Define constants
    K_FACTORS = 100 # The number of dimensional embeddings for movies and users
    RNG_SEED = 46
    EPOCH_COUNTER = 1
    WEIGHTS_PATH = 'models/RecSystem.h5'

    def __init__(self):
        logger.info("RecomendationEngineNN - created")

    def train(self, visits_df):
        logger.info("train() - starting")

        max_userid = visits_df['UID'].drop_duplicates().max()
        max_eventid = visits_df['EID'].drop_duplicates().max()

        model = self.__build_model(max_userid, max_eventid)

        callbacks = [EarlyStopping('val_loss', patience=2), ModelCheckpoint(self.WEIGHTS_PATH, save_best_only=True)]
        Users, Events, Visits = self.__shuffle_dataframes(visits_df)

        history = model.fit([Users, Events], Visits, epochs=self.EPOCH_COUNTER, validation_split=.1, verbose=2, callbacks=callbacks)
        self.__calculate_error(history)

        model = None
        self.__reset_keras()


    def predict(self, uid, visits_df):
        logger.debug("predict() - %s", uid)

        max_userid = visits_df['UID'].drop_duplicates().max()
        max_eventid = visits_df['EID'].drop_duplicates().max()

        model = self.__build_model(max_userid, max_eventid)

        model.load_weights(self.WEIGHTS_PATH)

        user_ratings = visits_df[visits_df['UID'] == uid][['UID', 'EID', 'rating']]

        recommendations = visits_df[visits_df['EID'].isin(user_ratings['EID']) == False][['EID']].drop_duplicates()
        recommendations['prediction'] = recommendations.apply(lambda x:self.__predict_rating(model, uid, x['EID']), axis=1)
        
        model = None
        self.__reset_keras()


        return recommendations

    def __predict_rating(self, model, user_id, item_id):
        return model.predict([np.array([user_id]), np.array([item_id])])[0][0]

        
    def __calculate_error(self, history):
        # Show the best validation RMSE
        min_val_loss, idx = min((val, idx) for (idx, val) in enumerate(history.history['val_loss']))
        print('Minimum RMSE at epoch', '{:d}'.format(idx+1), '=', '{:.4f}'.format(math.sqrt(min_val_loss)))


    def __shuffle_dataframes(self, visits_df):
        shuffled_ratings = visits_df.sample(frac=1., random_state=self.RNG_SEED)
        # Shuffling users
        Users = shuffled_ratings['UID'].values
        # Shuffling events
        Events = shuffled_ratings['EID'].values
        # Shuffling ratings
        Visits = shuffled_ratings['rating'].values

        return Users, Events, Visits


    def __build_model(self, max_userid, max_eventid):
        user_input = Input((1,))
        item_input = Input((1,))
        user_embedding = Flatten()(Embedding(max_userid, self.K_FACTORS, input_length=1)(user_input))
        item_embedding = Flatten()(Embedding(max_eventid, self.K_FACTORS, input_length=1)(item_input))
        loss = Dot(axes=1)([user_embedding, item_embedding])

        model = Model([user_input, item_input], loss)

        model.compile(loss='mse', optimizer="adamax")

        return model

    def __reset_keras(self):
        sess = get_session()
        clear_session()
        sess.close()
        gc.collect()


