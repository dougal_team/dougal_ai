import logging
import pandas as pd
import numpy as np
import sqlite3
from dougal.services.recomendation import RecomendationEngineNN

logger = logging.getLogger()

class DataHandler(object):
    """
    Facade for data handling services
    """
    DB_NAME = "dataset.db"

    def __init__(self):
        logger.info("DataHandler() - created")
        self.recomender = RecomendationEngineNN()
        

    def process_dataset(self,  u_path, e_path, v_path):
        logger.info("process_dataset() - starting for %s", v_path)

        users_df = pd.read_csv(u_path)
        events_df = pd.read_csv(e_path)
        visits_df = pd.read_csv(v_path)

        self.save_to_db(users_df, events_df, visits_df)

        self.recomender.train(visits_df)
        users_to_analyze = self.__get_users_to_analyze(events_df)
        self.__make_predictions_for_users(users_to_analyze)
        self.__update_dataset_status()
        self.__update_job_status()
        #self.recomender.predict(2000, visits_df, events_df)


    def save_to_db(self, users_df, events_df, visits_df):
        """
        Saves users data to system database
        """
        self.conn = sqlite3.connect(self.DB_NAME)

        users_df.to_sql("Users", self.conn, if_exists="replace")
        events_df.to_sql("Events", self.conn, if_exists="replace")
        visits_df.to_sql("Visits", self.conn, if_exists="replace")

        self.conn.commit()
        self.conn.close()

    def __get_users_to_analyze(self, users_df):
        # TODO: add smart logic for getting users ready for prediction
        #return users_df.groupby(['UID']).sum()
        return users_df

    def __make_predictions_for_users(self, users_df):
        pass

    def __update_dataset_status(self):
        pass
    
    def __update_job_status(self):
        pass


        



        


